﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Npgsql;
using System.Data;
using Inventory.Models;

namespace Inventory.Controllers
{
    public class InventoryController : Controller
    {
        // GET: Inventory
        public ActionResult Inventory(string cat)
        {


            if (cat != null)
                return RedirectToAction("CatgegoryInventory",new { cat = cat });
            else          
            {
                List<ItemModel> displayitem = SelectAll();
                return View(displayitem);
            }

        }

           
        public ActionResult CatgegoryInventory(string cat)
        {
            List<ItemModel> displayitem = SelectByCategory(cat);
            return View(displayitem);

        }

        public List<ItemModel> SelectAll()
        {
            List<ItemModel> displayitem = new List<ItemModel>();
            List<int> catIndex = new List<int>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
            conn.Open();
            NpgsqlCommand comm = new NpgsqlCommand();
            comm.Connection = conn;
            comm.CommandType = CommandType.Text;
            comm.CommandText = "Select * from items";

            NpgsqlDataReader sdr = comm.ExecuteReader();

            while (sdr.Read())
            {
                var itemlist = new ItemModel();
                itemlist.itemid = Convert.ToInt32(sdr["itemid"]);
                itemlist.itemname = sdr["itemname"].ToString();
                itemlist.description = sdr["description"].ToString();
                itemlist.buyprice = Convert.ToDouble(sdr["buyprice"]);
                itemlist.sellprice = Convert.ToDouble(sdr["sellprice"]);
                itemlist.added = Convert.ToDateTime(sdr["added"]);
                itemlist.expirationdate = Convert.ToDateTime(sdr["expirationdate"]);
                itemlist.category = sdr["category"].ToString();
                displayitem.Add(itemlist);
            }
            return displayitem;
        }
        public List<ItemModel> SelectByCategory(string cat)
        {
            List<ItemModel> displayitem = new List<ItemModel>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
            conn.Open();
            NpgsqlCommand comm = new NpgsqlCommand();
            comm.Connection = conn;
            comm.CommandType = CommandType.Text;
            comm.CommandText = "Select * from items where category = '" + cat + "'";
          
            NpgsqlDataReader sdr = comm.ExecuteReader();
       
            while (sdr.Read())
            {
                var itemlist = new ItemModel();
                itemlist.itemid = Convert.ToInt32(sdr["itemid"]);
                itemlist.itemname = sdr["itemname"].ToString();
                itemlist.description = sdr["description"].ToString();
                itemlist.buyprice = Convert.ToDouble(sdr["buyprice"]);
                itemlist.sellprice = Convert.ToDouble(sdr["sellprice"]);
                itemlist.added = Convert.ToDateTime(sdr["added"]);
                itemlist.expirationdate = Convert.ToDateTime(sdr["expirationdate"]);
                itemlist.category = sdr["category"].ToString();
                displayitem.Add(itemlist);
            }

            return displayitem;
        }

      
    }
}