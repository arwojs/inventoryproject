﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventory.Models;
using Npgsql;
using System.Data;

namespace Inventory.Controllers
{
    public class WykazController : Controller
    {
        // GET: Wykaz
        public ActionResult Wykaz()
        {
            List<WykazModel> displayitem = SelectAll();
            return View(displayitem);
        }

        public List<WykazModel> SelectAll()
        {
            List<WykazModel> displayitem = new List<WykazModel>();
            List<int> catIndex = new List<int>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
            conn.Open();
            NpgsqlCommand comm = new NpgsqlCommand();
            comm.Connection = conn;
            comm.CommandType = CommandType.Text;
            comm.CommandText = "Select * from wykaz";

            NpgsqlDataReader sdr = comm.ExecuteReader();

            while (sdr.Read())
            {
                var itemlist = new WykazModel();
                itemlist.id = Convert.ToInt32(sdr["id"]);
                itemlist.username = sdr["username"].ToString();               
                itemlist.buyprice = Convert.ToDouble(sdr["buyprice"]);
                itemlist.sellprice = Convert.ToDouble(sdr["sellprice"]);                
                itemlist.deldate = Convert.ToDateTime(sdr["deldate"]);               
                displayitem.Add(itemlist);
            }
            return displayitem;
        }
    }
}