﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventory.Models;
using Npgsql;
using System.Data;

namespace Inventory.Controllers
{
    public class AddDeleteController : Controller
    {
        // GET: AddDelete
        public ActionResult ItemTable()
        {         
                List<ItemModel> displayitem = SelectAll();
                return View(displayitem);          
        }


        public List<ItemModel> SelectAll()
        {
            List<ItemModel> displayitem = new List<ItemModel>();
          
            NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
            conn.Open();
            NpgsqlCommand comm = new NpgsqlCommand();
            comm.Connection = conn;
            comm.CommandType = CommandType.Text;
            comm.CommandText = "Select * from items";

            NpgsqlDataReader sdr = comm.ExecuteReader();

            while (sdr.Read())
            {
                var itemlist = new ItemModel();
                itemlist.itemid = Convert.ToInt32(sdr["itemid"]);
                itemlist.itemname = sdr["itemname"].ToString();
                itemlist.description = sdr["description"].ToString();
                itemlist.buyprice = Convert.ToDouble(sdr["buyprice"]);
                itemlist.sellprice = Convert.ToDouble(sdr["sellprice"]);
                itemlist.added = Convert.ToDateTime(sdr["added"]);
                itemlist.expirationdate = Convert.ToDateTime(sdr["expirationdate"]);
                itemlist.category = sdr["category"].ToString();
                displayitem.Add(itemlist);
            }
            return displayitem;
        }

        public ActionResult Delete(int itemId = 9999)
        {
           
                addDelLog(itemId);
                NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
                conn.Open();
                NpgsqlCommand comm = new NpgsqlCommand("DELETE FROM items WHERE itemid = '" + itemId + "'", conn);

                NpgsqlDataReader sdr = comm.ExecuteReader();
                while (sdr.Read())
                {
                }
                return RedirectToAction("ItemTable");
         
        }

        public void addDelLog(int itemid)
        {
            List<WykazModel> displayitem = new List<WykazModel>();
            List<int> catIndex = new List<int>();
            NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
            conn.Open();
            NpgsqlCommand comm = new NpgsqlCommand("Select buyprice, sellprice from items where itemid = '" + itemid + "'", conn);
            NpgsqlDataReader sdr = comm.ExecuteReader();
            var today = DateTime.Now;
            var stoday = today.Date;
            var itemlist = new WykazModel();
            while (sdr.Read())
            {
                
                itemlist.username = Session["username"].ToString();
              
                itemlist.buyprice = Convert.ToDouble(sdr["buyprice"]);
                itemlist.sellprice = Convert.ToDouble(sdr["sellprice"]);
                itemlist.deldate = stoday;                
                displayitem.Add(itemlist);
            }
            conn.Close();
            conn.Open();        
        
            NpgsqlCommand comm2 = new NpgsqlCommand("INSERT INTO public.wykaz(username, buyprice, sellprice, deldate) VALUES('" + itemlist.username + "', '" + itemlist.buyprice + "', '" + itemlist.sellprice + "', '" + itemlist.deldate +"')", conn);

            NpgsqlDataReader sdr2 = comm2.ExecuteReader();
            while (sdr2.Read())
            {
            }

        }

        public ActionResult AddView()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddItem(ItemModel item)
        {
                NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
                conn.Open();
                NpgsqlCommand comm = new NpgsqlCommand("Select MAX(itemid) from items ", conn);
                NpgsqlDataReader sdr = comm.ExecuteReader();
                var newitemid = 9999;
                while (sdr.Read())
                {
                    newitemid = Convert.ToInt32(sdr["max"]) + 1;
                }
                conn.Close();
                conn.Open();
                var today = DateTime.Now;
                var stoday = today.Year + "-" + today.Month + "-" + today.Day;
                var expire = item.expirationdate;
                var sexpire = expire.Year + "-" + expire.Month + "-" + expire.Day;
                NpgsqlCommand comm2 = new NpgsqlCommand("INSERT INTO items(itemid, itemname, description, buyprice, sellprice, added, expirationdate, category) VALUES('" + newitemid + "' ,'" + item.itemname + "' ,'" + item.description + "' ,'" + item.buyprice + "' ,'" + item.sellprice + "' ,'" + stoday + "' ,'" + sexpire + "' ,'" + item.category + "')", conn);

                NpgsqlDataReader sdr2 = comm2.ExecuteReader();
                while (sdr2.Read())
                {
                }
                return RedirectToAction("ItemTable");          
        }
    }
}