﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Npgsql;
using System.Data;
using Inventory.Models;


namespace Inventory.Controllers
{
    public class UserController : Controller
    {

        // GET: User
        public ActionResult Index(UserModel acc)
        {
           
            if(acc.username != null)
            {
                return RedirectToAction("Verify",  acc );
            }
       
            return View();           
        }

        [HttpGet]
        public ActionResult Menu(string idt)
        {
            if (Session["username"] != null)
            {
                if (idt != null)
                    return RedirectToAction("MenuChoice", new { idt = idt });
                else
                {

                    return View();
                }
            }
            else return RedirectToAction("Index");
        }
      

        [HttpGet]
        public ActionResult Login(UserModel objUser)
        {          
            return View(objUser);
        }

        
         [HttpPost]
      //[ValidateAntiForgeryToken]
        public ActionResult Verify(UserModel acc)
        {
          
            
                NpgsqlConnection conn = new NpgsqlConnection("Server=ec2-54-73-58-75.eu-west-1.compute.amazonaws.com;Port=5432;Database=d87efif24kvo7c;User Id=caokydqnzmfkji;Password=5424f0c134b63cc3e18b52cb8867dba4a3bc09214538e57b8efc13d418d33257;SSL Mode=Require;TrustServerCertificate=True;");
                conn.Open();
                NpgsqlCommand comm = new NpgsqlCommand();
                comm.Connection = conn;
                comm.CommandType = CommandType.Text;
                comm.CommandText = "Select * from users where username='" + acc.username + "' and password='" + acc.password + "'";
                NpgsqlDataReader sdr = comm.ExecuteReader();

                if (sdr.Read())
                {

                    conn.Close();
                    Session["username"] = acc.username;
                    return RedirectToAction("Menu");
                }
                else
                {
                    conn.Close();
                    return Redirect("/Home/Index");
                }
         
        }


     

        public ActionResult MenuChoice(string idt)
        {
            switch(idt)
            {
                case "1":
                    return Redirect("/AddDelete/ItemTable");
                case "2":
                    return Redirect("/Inventory/Inventory");
                case "3":
                    return Redirect("/Wykaz/Wykaz");
                default:
                    return RedirectToAction("Menu");
            }

        }
    }
}