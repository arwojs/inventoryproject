﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Inventory.Models
{
    public class ItemModel
    {
        [Key]
        public int itemid { get; set; }
        public string itemname { get; set; }
        public string description { get; set; }
        public double buyprice { get; set; }
        public double sellprice { get; set; }
        public DateTime added { get; set; }
        public DateTime expirationdate { get; set; }
        public string category { get; set; }


    }
}