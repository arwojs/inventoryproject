﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Inventory.Models
{
    public class WykazModel
    {
        [Key]
        public int id { get; set; }
        public string username { get; set; }      
        public double buyprice { get; set; }
        public double sellprice { get; set; }
        public DateTime deldate { get; set; }


    }
}