﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Inventory.Models
{
    public class UserModel
    {
        [Key]
        public int id { get; set; }       
        public string password { get; set; }
        public string username { get; set; }
    }
}